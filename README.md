# heroku-buildpack-sbcl

Buildpack for SBCL on Heroku.

This is a modified version of [https://github.com/yangby/heroku-buildpack-sbcl](https://github.com/yangby/heroku-buildpack-sbcl).

This version properly caches the quicklisp builds, allowing you to avoid the dreaded
[R10 - Boot timeout](https://devcenter.heroku.com/articles/error-codes#r10-boot-timeout).

## Environment

### The Operating System

- [The Heroku-18 Stack](https://devcenter.heroku.com/articles/stack "Stacks")

### Requirements / Dependencies

- [Steel Bank Common Lisp (SBCL)](http://sbcl.org/ "A high performance Common Lisp compiler")

- [Quicklisp](https://www.quicklisp.org/ "A library manager for Common Lisp")

- [Another System Definition Facility (ASDF)](https://www.common-lisp.net/project/asdf/ "Another System Definition Facility")

- [heroku-buildpack-apt](https://github.com/heroku/heroku-buildpack-apt.git)
This is necessary to properly run CLSQL on postgres on The Heroku-18.

## Usage

This assumes you have a ASDF project which you can properly load via quicklisp locally.

Write a file named _build.lisp_ in the root of your source tree to build the
system.

```lisp

;;; Pre-compile our libs
(in-package :cl-user)

;; This is the only variable part.
(defvar *project* :my-asdf-project) ; change for a different project.

(log-title "Building Common Lisp...")
(log-content "Pre-Compiling project...")
(push (format nil "~A/" *build-dir*) asdf:*central-registry*)
(ql:quickload *project*)

;;; Pre-compile CLSQL postgres driver
(log-content "Building CLSQL for postgres ...")
(ql:quickload :clsql)
;; Do a junk connect to force the compilation.
(ignore-errors
 (clsql:connect '("localhost" "blah" "postgres" "blah")
                :database-type :postgresql))
```

(*Optional*) Write a file named _build.env_ in the root of your source tree
to define environment variables.

```bash
export SBCL_VERSION="sbcl-1.3.17-x86-64-linux"
```

Write a file named _Procfile_ in the root of your source tree to start the
server.

```
web: sbcl --script web-launcher.lisp
```

Write a file named _web-launcher.lisp_ in the root of your source tree for
_Procfile_ to execute.

```lisp
(in-package :cl-user)

(defvar *virtual-root*
  (pathname (concatenate
             'string (asdf::getenv "VIRTUAL_ROOT") "/")))
(defvar *sbcl-home*
  (pathname (concatenate
             'string (asdf::getenv "SBCL_HOME") "/")))
(defvar *port* (parse-integer (asdf::getenv "PORT")))

(log-title "Checking environment variables ...")
(log-content "VIRTUAL_ROOT = ~a" *virtual-root*)
(log-content "   SBCL_HOME = ~a" *sbcl-home*)
(log-content "        PORT = ~d" *port*)
(log-footer "")

(defun require-quicklisp ()
  (let ((quicklisp-init
         (merge-pathnames "quicklisp/setup.lisp" *virtual-root*)))
    (when (probe-file quicklisp-init)
      (load quicklisp-init))))

(require-quicklisp)

(log-title "Check SBCL-Framework before running ...")
(log-content "        SBCL Version ~a." (lisp-implementation-version))
(log-content "        ASDF Version ~a." (asdf:asdf-version))
(log-content "   Quicklisp Version ~a." (quicklisp-client:client-version))
(log-footer "")

;;; Setup our local project
(log-title "Setting up *central-registry*...")
(log-content "        CWD: ~A" "/app/")
(log-content "        Adding to registry: ~A" "/app/")
(log-footer "")
(push "/app/" asdf:*central-registry*)

;;; Load it
(require :my-asdf-project) ; change me
(in-package :my-asdf-project) ; change me

(defvar *port* (parse-integer (asdf::getenv "PORT")))
(lucerne:start my-asdf-project:main :port *port*)

;; I can't get woo to work on heroku...
;;(lucerne:start my-asdf-project:main :port *port* :debug t)

;; We must keep this.
(loop (sleep 1000))
```

Finally, to use CLSQL (and possibly other Postgres libs),
you'll need to include
[heroku-buildpack-apt](https://github.com/heroku/heroku-buildpack-apt.git)
in the build packs and add an _Aptfile_ with the following content:

```
libpq-dev
libpq5
```

### Examples

- [Run Hunchentoot](https://github.com/yangby/heroku-example-sbcl-hunchentoot)

## References

- [Buildpack API | Heroku Dev Center](https://devcenter.heroku.com/articles/buildpack-api)

- [Buildpacks | Heroku Dev Center](https://devcenter.heroku.com/articles/buildpacks)

- [Third-Party Buildpacks | Heroku Dev Center](https://devcenter.heroku.com/articles/third-party-buildpacks)

- [Process Types and the Procfile | Heroku Dev Center](https://devcenter.heroku.com/articles/procfile)

- [mtravers/heroku-buildpack-cl](https://github.com/mtravers/heroku-buildpack-cl)

- [avodonosov/heroku-buildpack-cl2](https://github.com/avodonosov/heroku-buildpack-cl2)

- [orivej/heroku-buildpack-cl](https://github.com/orivej/heroku-buildpack-cl)

- [ddollar/heroku-buildpack-apt](https://github.com/ddollar/heroku-buildpack-apt)
